import { Book } from './../../../models/book.model';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Nullable } from 'src/app/shared/types';
@Component({
    selector: 'app-book-list',
    templateUrl: './book-list.component.html',
    styleUrls: ['./book-list.component.scss']
})
export class BookListComponent {
    @Input() books: Nullable<Book[]>;
    @Output() add = new EventEmitter<string>();
}
