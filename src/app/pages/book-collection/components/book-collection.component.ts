import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Book } from 'src/app/models/book.model';
import { Nullable } from 'src/app/shared/types';

@Component({
  selector: 'app-book-collection',
  templateUrl: './book-collection.component.html',
  styleUrls: ['./book-collection.component.scss']
})
export class BookCollectionComponent {
    @Input() books: Nullable<Book[]>;
    @Output() remove = new EventEmitter<string>();
}
