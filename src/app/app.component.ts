import { AppState } from './store/app.state';
import { addBook, removeBook, loadBook } from './store/actions/books.actions';
import { selectBookCollection, selectBooks } from './store/selectors/books.selectors';
import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Book } from './models/book.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    books$: Observable<Book[]> = this._store.pipe(select(selectBooks));
    bookCollection$: Observable<Book[]> = this._store.pipe(select(selectBookCollection));
    constructor(private _store: Store<AppState>
    ) {}

    ngOnInit(): void {
            this._store.dispatch(loadBook());
    }

    onAdd(bookId: string): void {
        this._store.dispatch(addBook({ bookId }));
      }

      onRemove(bookId: string): void {
        this._store.dispatch(removeBook({ bookId }));
      }
}
