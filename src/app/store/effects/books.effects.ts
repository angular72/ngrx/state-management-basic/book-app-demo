import { loadBookSuccess, loadBookError } from './../actions/books.actions';
import { BookService } from './../../services/books.service';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap, tap } from 'rxjs/operators';
import { Book } from 'src/app/models/book.model';
import { of } from 'rxjs';

@Injectable()
export class BookEffects {
    loadBooks$ = createEffect( () => this._actions$.pipe(
        ofType('[Book List] Load Book'),
        mergeMap( () => this._bookService.getBooks().pipe(
            map( ( books: Book[] ) => loadBookSuccess({ books })),
            catchError( () => of(loadBookError()))
        ))
    ));

    constructor(
        private _actions$: Actions,
        private _bookService: BookService
    ) {}
}
