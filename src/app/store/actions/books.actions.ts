import { createAction, props } from '@ngrx/store';
import { Book } from 'src/app/models/book.model';

export const loadBook = createAction(
    '[Book List] Load Book'
);

export const loadBookSuccess = createAction(
    '[Book List] Load Book Success',
    props<{ books: Book[] }>()
);

export const loadBookError = createAction(
    '[Book List] Load Book Error'
)

export const addBook = createAction(
    '[Book List] Add Book',
    props<{ bookId: string }>()
);

export const removeBook = createAction(
    '[Book Collection] Remove Book',
    props<{ bookId: string }>()
);

export const retrievedBookList = createAction(
    '[Book List/API] Retrieve Books Success',
    props<{ books: Book[] }>()
);
