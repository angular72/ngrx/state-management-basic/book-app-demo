import { createSelector, createFeatureSelector } from '@ngrx/store';
import { Book } from 'src/app/models/book.model';
import { AppState } from '../app.state';

export const selectBooks = createSelector(
    ( state: AppState ) => state.books,
    ( books: Book[] ) => books
);

export const selectCollectionState = createFeatureSelector<
    AppState,
    string[]
>('collection');

export const selectBookCollection = createSelector(
    selectBooks,
    selectCollectionState,
    ( books: Book[], collection: string[] ) =>
            collection.map( ( id: string ) => books.find( ( book: Book ) => book.id === id)) as Book[]
    );
