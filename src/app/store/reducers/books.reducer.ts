import { loadBook, loadBookSuccess, loadBookError } from './../actions/books.actions';
import { createReducer, on } from '@ngrx/store';
import { Book } from '../../models/book.model';
import { retrievedBookList } from '../actions/books.actions';

export const initialState: Book[] = [];

export const booksReducer = createReducer(
    initialState,
    on(retrievedBookList, ( state, { books } ) => [...books]),
    on(loadBook, ( state ) => [...state]),
    on(loadBookSuccess, ( state, { books } ) => [...state, ...books]),
    on(loadBookError, ( state ) => [...state])
);
